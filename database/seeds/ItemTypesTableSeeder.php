<?php

use Illuminate\Database\Seeder;

class ItemTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('item_types')->insert([
        	'item_type' => 'Office Supplies'
        ]);

        DB::table('item_types')->insert([
        	'item_type' => 'Equipments'
        ]);

        DB::table('item_types')->insert([
        	'item_type' => 'Janitorial Supplies'
        ]);
    }
}
