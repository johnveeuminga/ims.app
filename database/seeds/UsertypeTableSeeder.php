<?php

use Illuminate\Database\Seeder;

class UsertypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('usertypes')->insert([
        	'usertype' => 'Administrator'
        ]);

        DB::table('usertypes')->insert([
        	'usertype' => 'Employee'
        ]);

    }
}
