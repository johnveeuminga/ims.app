<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('employees')->insert([
        	'office_id' => 1,
        	'created_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
            'name' => 'Employee Blabla',
            'username' => 'employee',
            'password' => bcrypt('password'),
            'userable_id' => 1,
            'userable_type' => 'App\Employee',
            'created_at' => Carbon::now(),
        ]);

        DB::table('employees')->insert([
        	'office_id' => 7,
        	'created_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
            'name' => 'Acad4 Employee',
        'username' => 'acad4',
            'password' => bcrypt('password'),
            'userable_id' => 2,
            'userable_type' => 'App\Employee',
            'created_at' => Carbon::now(),
        ]);
    }
}
