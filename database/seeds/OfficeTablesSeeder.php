<?php

use Illuminate\Database\Seeder;

class OfficeTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('offices')->insert([
            'office_name' => 'Head HAG'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'Deputy Head HAG'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'Executive Office'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'Acad1'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'Acad2/3'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'Acad4'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'Acad5'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'Acad6'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'MFO/Budget'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'Sergeant Major'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'DNS'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'DMgt'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'Dmat/Dcis'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'DHum'
        ]);

        DB::table('offices')->insert([
            'office_name' => 'DSS'
        ]);
    
    }
}
