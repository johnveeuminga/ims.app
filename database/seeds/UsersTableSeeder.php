<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$now = Carbon::now();
        DB::table('users')->insert([
            'username' => 'admin',
            'password' => bcrypt('password'),
            'office_id' => 6,
            'created_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
            'username' => 'emp1',
            'password' => bcrypt('password'),
            'office_id' => 7,
            'created_at' => Carbon::now(),
        ]);
    }
}
