<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([
            'item_name' => 'Acetate',
            'amount' => 200,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'MTR',
            'reorder_date' => Carbon::now(),
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Ballpen, Black (Pilot)',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ballpen, Blue (Pilot)',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',
        ]);

        DB::table('items')->insert([
            'item_name' => 'Ballpen, Red (Pilot)',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Battery, "AAA"',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Battery 1.5 9v',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Battery 23A',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Battery 1.5D cell',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Battery C"',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Board Illustration Whole',
            'amount' => 15,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Book, Record 500pp',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BK',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Calculator',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Cartolina Asst Colors',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Chalk, White',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BX',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Chalk, Colored',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BX',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Clip Board',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Clip Paper, Small',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BX',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Clip Paper, Big',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BX',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Correction Fluid',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BTL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Correction Tape',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Crayons',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BX',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Crayons Pastel',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BX',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Cutter',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Cutterblade, Big',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Envelope, Doc Long',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Envelope, Doc Short',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Envelope, Expanding Long',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Envelope, Mailing White Short',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Eraser, Blackboard',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Fastener, Plastic',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Folder, File, White Long',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Folder, File White A4',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Folder, Plastic w/ Slide Long',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Folder, Plastic w/ Slide Short',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Folder Organizer',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Foold Back Clips 1"',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Fold Back Clips 2',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Glue, Elmers 130gms',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BTL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Index Card 5x8',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink, Riso',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'TB',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink, Stamp Pad',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BTL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink, Refil, Whiteboard Blue',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BTL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink, Refil, Whiteboard Black',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BTL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink, Refil, Whiteboard Red',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BTL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Marking Pen Permanent Black',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Marking Pen Permanent Blue',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Marking Pen Permanent Red',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Marking Pen Whiteboard Black',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Marking Pen Whiteboard Blue',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Marking Pen Fluorescent',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Notebook',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BK',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Notepad, Self-stick 2x3',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PD',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper,Construction',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper Copier Long',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'RM',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper Copier A4',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'RM',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper Manila',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper Morocco',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper, Specialty Short',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper, Parchment Short',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper Sticker',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper, Photo',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Pencil #2',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Pen, Projector, Fine (OHP)',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'SET',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Pin, Map',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BX',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Puncher HD',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'RISO Master',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'RLL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ruler Metal 24"',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Sharpener, HD',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Scissors',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PR',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Sign Pen, Blue',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Stamp Pad',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Staple, HD big',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Staple Wire #35',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BX',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Staple Wire #66',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BX',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Staple Wire #10',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BX',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Staple Remover',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Tape, Dispenser Standard',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Tape, Double Sided Green',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Tape, Double Sided 1"',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'RLL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Tape, Transparent 1/2',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'RLL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Tape, Transparent 1"',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'RLL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Tape, Duct',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'RLL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Tape Masking 1"',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'RLL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Tape Masking 2"',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'RLL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Tape Packing 2"',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'RLL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Tarnsparency Film',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Thumbtacks',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BX',

        ]);

        DB::table('items')->insert([
            'item_name' => 'USB 16GB',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'CD, Recordable',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'CD, Re-writable',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'DVD, Recordable',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink Cart HP 21',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink Cart HP 22',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink Cart Canon #810',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink Cart Canon #811',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink Cart Canon #830',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ribbon Epson LX 300',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink, Refil Canon Black',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'LTR',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink, Refil Canon Magenta',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'LTR',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink, Refil Canon Yellow',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'LTR',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink, Refil Canon Black 100ML',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BTL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink, Refil Canon Magenta 100ML',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BTL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink, Refil Canon Yellow 100ML',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BTL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ink, Refil Canon Cyan 100ML',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BTL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Tucker Gun',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PCS',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Tucker Wire',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PCS',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Vernier Caliper',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'WD-40',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Binoculars',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Rubber Band',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'BX',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper Gift Bag Large',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PCS',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper Gift Bag Medium',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PCS',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper Gift Bag Small',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PCS',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Gift Wrapper w/ HAG Seal',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PCS',

        ]);

        DB::table('items')->insert([
            'item_name' => 'PMA Portrait',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Thermo Mugs',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Frame, 8.5x11',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Nipa Hut',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Badges (Superintendent)',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Medal, Gold',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'MEdal, Silver',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Medal, Bronze',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Medal, Star',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 1,
            'unit' => 'PC',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Air Freshner 500ML',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'BTL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Alcohol, 500ML',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'BTL',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Aluminom Foil',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'ROLL',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Basin',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PC',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Baygon',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'BTL',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Broom, Soft',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'EA',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Broom, Stick',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'EA',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Brush, Toilet Bowl',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'EA',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Brush, Floor w/ Handle',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'EA',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Brush, Floor Polisher 17"',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'EA',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Cleanser Powder 500g',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'CAN',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Cling Wrap',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'RLL',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Cotton Small',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PK',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Deodorant Cake',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'EA',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Deep Plastic Tray',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PC',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Dishwashing Liquid',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'BTL',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Dishwashing Paste',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'CAN',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Doormat Rubberized',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PC',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Doormat Cotton',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PC',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Doormat (mix)',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'EA',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Dustpan, Plastic',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'EA',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Downy',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'BTL',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Floorwax 2kg Red',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'CAN',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Floorwax Natural 2kg',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'CAN',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Floorwax Natural 2.8kg',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'CAN',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Floorwax Natural 450g',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'CAN',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Floorwax Paste Red',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'GAL',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Floorwax Natural',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'GAL',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Glass Cleaner, 500ML',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'BTL',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Gloves',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PR',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Metal Polish',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'CAN',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Mophandle, Screw Type, Wood',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'EA',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Mophandle Plastic',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'EA',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Mophead Cotton Thread',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'EA',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Mop Foam Head',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'EA',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Muriatic Acid',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'GAL',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Muriatic Acid',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'LTR',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper Plates',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PKS',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Paper Cups',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PK',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Pump Rubber',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PC',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Rags',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PC',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Scotchbrite',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PC',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Scrubbing Pad Floor',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PC',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Soap, Handwash',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'BTL',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Soap, Powder 500g',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'EA',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Styro Cups',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PK',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Trash Bag, Plastic Large',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PC',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Trash Can',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PC',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'True Coat Wax Red 450G',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'CAN',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'True Coat Wax Red 1K',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'CAN',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'True Coat Green 1kl',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'CAN',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Tissue, Paper',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'RLL',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Tissue, Paper Towel',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PK',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Waste Basket w/ Cover',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PC',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Water Dipper',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PC',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Water Container',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PC',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Zonrox ltr',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'BTL',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'Zonrox',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'GAL',
            
        ]);

        DB::table('items')->insert([
            'item_name' => 'WD40',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 3,
            'unit' => 'PCS',
        ]);

         DB::table('items')->insert([
            'item_name' => 'Riffle 5.56mm M16A1',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 1,
            'unit' => 'EA',

        ]);
         DB::table('items')->insert([
            'item_name' => 'Sub Machine Gun 5.56mm M653',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 1,
            'unit' => 'EA',

        ]);
         DB::table('items')->insert([
            'item_name' => 'Pistol Caliber 45 M1911A1',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 1,
            'unit' => 'EA',

        ]);
         DB::table('items')->insert([
            'item_name' => 'Ctg Cal 45 Ball rds',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 1,
            'unit' => 'EA',

        ]);
         DB::table('items')->insert([
            'item_name' => 'Ctg 5.56mm Ball',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 1,
            'unit' => 'EA',

        ]);
         DB::table('items')->insert([
            'item_name' => 'Magazine (30 rds cap) Aluminum',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 1,
            'unit' => 'EA',

        ]);
         DB::table('items')->insert([
            'item_name' => 'Magazine (20 rds cap)',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 1,
            'unit' => 'EA',

        ]);

         DB::table('items')->insert([
            'item_name' => 'Magazine for Cal. 45',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 1,
            'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Toyota Innova',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 2,
            'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Van Delica',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 2,
            'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Public Address System',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 3,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Telephone Sets (analog, digital',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 3,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Desktop Computer',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 4,
        'unit' => 'EA',

        ]);
        DB::table('items')->insert([
            'item_name' => 'Laptop',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 4,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Scanner',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 4,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'LCD Projector',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 4,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Projector Screen, White',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 4,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Smart TV',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 4,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Computer Printer w/ Scanner & Xerox',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 4,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Speaker for Classrooms',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 4,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Power Amplifier and Mixer',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 4,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Wireless Access Point',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 4,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Risograph Machine (Heavy Duty)',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 4,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Photocopying Machine',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 4,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Uninterrupted Power Supply',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 5,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Power Generator (250 KVA)',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 5,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Hubs/Switches',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 6,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Media Converter',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 6,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Cabinet Filing, Steel',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Cabinet Table',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Cabinet Shelve Made of Wood',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Book shelve, Cream, Back to Back',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Chair, Classroom',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Chair, Computer/Ergonomic',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Chair, Jr Executive',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Chair, Senior Executive',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Chair, Stacking',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Chair, Fiber with Armrest',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Chair, Monoblock',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Integrated Rostrum (IR)',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Table, Classroom',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Table, Jr Executive',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Office/Clerical Table',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Dining/Conference Table ',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Sofa/Sala Set',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Computer Table',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 7,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Aircondition',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 8,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ceiling/Standard Fan',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 8,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Vacuum Cleaner',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 8,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Television Colored',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 8,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Refrigirator',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 8,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Alarm Ringer',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 9,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Biometric',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 9,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Speaker for Classroom',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 9,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Fire Extinguisher',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 10,
        'unit' => 'EA',

        ]);

        DB::table('items')->insert([
            'item_name' => 'Ladder',
            'amount' => 100,
            'stock_danger' => 10,
            'item_types_id' => 2,
            'equipment_type_id' => 10,
        'unit' => 'EA',

        ]);

    }
}