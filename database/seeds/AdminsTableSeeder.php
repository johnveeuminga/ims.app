<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
        	'created_at' => Carbon::now(),
        ]);

       	DB::table('users')->insert([
            'name' => 'Nordeliza Calsie',
            'username' => 'admin',
            'password' => bcrypt('password'),
            'userable_id' => 1,
            'userable_type' => 'App\Admin',
            'created_at' => Carbon::now(),
        ]);
    }
}
