<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsertypeTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ItemTypesTableSeeder::class);
        $this->call(ItemsTableSeeder::class);
        $this->call(OfficeTablesSeeder::class);
        $this->call(EquipmentTypesTableSeeder::class);
        
    }
}
