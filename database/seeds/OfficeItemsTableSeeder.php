<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class OfficeItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('office_items')->insert([
            'item_id' => 1,
            'amount' => 2,
            'withdraw_id' => 1,

        ]);

         DB::table('office_items')->insert([
            'item_id' => 2,
            'amount' => 2,
            'withdraw_id' => 1,

        ]);

        DB::table('office_items')->insert([
            'item_id' => 2,
            'amount' => 2,
            'withdraw_id' => 3,

        ]);

        DB::table('office_items')->insert([
            'item_id' => 1,
            'amount' => 2,
            'withdraw_id' => 3,

        ]);
    }
}
