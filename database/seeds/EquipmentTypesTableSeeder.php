<?php

use Illuminate\Database\Seeder;

class EquipmentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('equipment_types')->insert([
        	'equipment_type' => 'Weapons & Ammunition'
        ]);

        DB::table('equipment_types')->insert([
        	'equipment_type' => 'Vehicle'
        ]);

        DB::table('equipment_types')->insert([
            'equipment_type' => 'Base Communication Equipment'
        ]);

        DB::table('equipment_types')->insert([
            'equipment_type' => 'IT Equipment'
        ]);

        DB::table('equipment_types')->insert([
            'equipment_type' => 'Power System'
        ]);

        DB::table('equipment_types')->insert([
            'equipment_type' => 'LAN'
        ]);

        DB::table('equipment_types')->insert([
            'equipment_type' => 'Furniture'
        ]);

        DB::table('equipment_types')->insert([
            'equipment_type' => 'Commercial Furnishing & Appliances'
        ]);

        DB::table('equipment_types')->insert([
            'equipment_type' => 'Office Machines, Visible Record Equipment & Data'
        ]);

        DB::table('equipment_types')->insert([
            'equipment_type' => 'Fire Fighting Equipment'
        ]);
    }
}
