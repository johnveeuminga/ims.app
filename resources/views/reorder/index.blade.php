@extends('layouts.master')
@section('content')
	@include('components.navbar')
	<div class="wrapper">
		@include('components.sidebar')
	</div>
	<div class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="page-title">Reorder Supplies</h2>
					<div class="form-container">
						@if(session('success'))
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									<span class="sr-only">Close</span>
								</button>
								<strong>Success!</strong> {{session('success')}}
							</div>
						@endif
						@if($errors->any())
							<div class="alert alert-danger" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								    <span aria-hidden="true">&times;</span>
								 </button>
							    <strong>Warning!</strong> Reorder cannot be processed.
							</div>
						@endif
						<form action="/reorder" method="POST">
							{{csrf_field()}}
							<div class="row">
								<div class="col-md-6">
									<div class="font-weight-bold">Search:</div>
									<div class="">
										<select class="select-item">
											<option value="">Search for an item to reorder..</option>
											@foreach($items as $item)
												<option value="{{$item->id}}">{{$item->item_name}}</option>
											@endforeach
										</select>
										<button class="btn btn-primary" type="button" id="add-item">Add</button>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 mt-4">
									<table class="table table-striped" id="reorder-table">
										<thead>
											<tr>
												<th>Item</th>
												<th>Current Quantity</th>
												<th>Reorder Quantity</th>
												<th>Remove</th>
											</tr>
										</thead>
										<tbody>
											
										</tbody>
									</table>
									<div class="float-sm-right button-container">
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script>
		$(document).ready(function(){
			$('#reorder').addClass('active');
				itemselect = $('.select-item').selectize({
			});

			$('#add-item').click(function(){
				itemSelectize  = itemselect[0].selectize;
				id = parseInt(itemSelectize.getValue());
				switch(id){
					@foreach($items as $item)
						case {{$item->id}}:
							if($("#qty{{$item->id}}").length == 0){
								$('#reorder-table tbody').append(
									"<tr id='{{$item->id}}'>"+
										'<th>'+"{{$item->item_name}}"+'</th>'+
										"<td>{{$item->amount}}</th>" + 
										"<td><input type='number' class='form-control qty' min='1' name='qty[]' step='1' id=qty"+{{$item->id}}+" value='1' />"+
										"<td><button type='button' class='btn btn-danger btn-remove' data-tr-target='{{$item->id}}'>REMOVE</button></td>"+
										'<input type="hidden" value="{{$item->id}}" name="items[]" />'+
									'</tr>'
								);
							}else{
								console.log('add');
								if($('#qty{{$item->id}}').val().length ==0){
									$("#qty{{$item->id}}").val(1);
								}else{
									$("#qty{{$item->id}}").val( function(i, oldval) {
									    return ++oldval;
									});
								}
							}

							break;
					@endforeach

					
				}
			});
			$(document).on("click", 'button.btn-remove', function(e){
				var id = ($(this).data('tr-target'));
				$('#reorder-table #'+id).remove();
			});

		})
	</script>
@endsection