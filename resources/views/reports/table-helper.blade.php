<table class="table table-striped report-table">
	<thead>
		<tr>
			<th>Item</th>
			@foreach($offices as $office)
				<th>{{$office->office_name}}</th>
			@endforeach
		</tr>
	</thead>
	<tbody>
		@foreach($items as $item)
		<tr>
			<th>{{$item->item_name}}</th>
			@foreach($offices as $office)
				@php
					$i=0;
				@endphp
				<td class="office-col text-center">
					@foreach($office->withdrawals as $withdrawal)
						@foreach($withdrawal->items as $withdrawalItem)
							@if($withdrawalItem->item_id == $item->id)
								@if($withdrawalItem->item->reorder_date)
									@if(\Carbon\Carbon::parse($withdrawalItem->created_at)->gte(\Carbon\Carbon::parse($withdrawalItem->item->reorder_date)))
										@php
											$i += $withdrawalItem->amount;
										@endphp
									@endif
								@else
									@php
										$i += $withdrawalItem->amount;
									@endphp
								@endif
							@endif
						@endforeach
					@endforeach
					@if(!$i==0)
						{{$i}}
					@endif
				</td>
			@endforeach
		</tr>
		@endforeach
	</tbody>
</table>