@extends('layouts.master')
@section('content')
@include('components.navbar')
	<div class="wrapper">
		@include('components.sidebar')
	</div>
	<div class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h4 class="page-title">Generate Reports</h4>
					<hr>
					<div class="dashboard-section">
						<div class="form-group">
							<label for="opion-select d-block">Pleaes Select a Report to Generate:</label>
							<select class="custom-select d-block" id="report-type">
								<option selected>Select a report...</option>
								<option value = 1>Monthly Withdrawal Summary</option>
								<option value = 2>Yearly Monthly Withdrawal Summary</option>
							</select>
							<div class="month-selector d-none">
								<div class="row">
									<div class="col-md-6">
										<div class="row">
											<div class="col-md-4">
												<label for="month" class="d-block">Month:</label>
												<select id="month" class="custom-select">
													<option value="0">Select a month...</option>
													<option value="1">January</option>
													<option value="2">February</option>
													<option value="3">March</option>
													<option value="4">April</option>
													<option value="5">May</option>
													<option value="6">June</option>
													<option value="7">July</option>
													<option value="8">August</option>
													<option value="9">September</option>
													<option value="10">October</option>
													<option value="11">November</option>
													<option value="12">December</option>
												</select>
											</div>
											<div class="col-md-6">
												<label for="month-year" class="d-block">Year:</label>
												<input class="form-control" type="number" min="1990" value="1990" id="month-year" style="max-width: 150px;" placeholder="Input a year"></input>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class="year-selector d-none">
								<div class="row">
									<div class="col-md-6">
								
										<label for="month-year" class="d-block">Year:</label>
										<input class="form-control" type="number" min="1990" value="1990" id="year" style="max-width: 150px;" placeholder="Input a year"></input>
										
									</div>
								</div>
							</div>
							<button type="button" class="btn btn-primary mt-2 d-none" id="generate">GENERATE</button>

							<div class="table-container">
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script>
		$('#reports').addClass('active');
		$('#report-type').change(function(){
			reportType = $(this).val();
			if(reportType == 1){
				$('.month-selector').removeClass('d-none')
				$('.month-selector').addClass('d-block')
				$('.year-selector').removeClass('d-block')
				$('.year-selector').addClass('d-none')
				$('.table-container').html('').show()
				$('#generate').removeClass('d-none');
			}else if(reportType == 2){
				$('.year-selector').removeClass('d-none')
				$('.year-selector').addClass('d-block')
				$('.month-selector').removeClass('d-block')
				$('.month-selector').addClass('d-none')
				$('#generate').removeClass('d-none');
				$('.table-container').html('').show()

			}else{
				$('.month-selector').removeClass('d-block')
				$('.month-selector').addClass('d-none')
				$('.year-selector').removeClass('d-block')
				$('.year-selector').addClass('d-none')
				$('.table-container').html('').show()
				$('#generate').addClass('d-none');
			}
		});
		$('#generate').on('click', function(){
			reportType = $('#report-type').val();
			if(reportType == 1){
				if($('#month').val()!=0 && ($('#month-year').val()>=1990 && $('#month-year').val()!=null)){
					$.ajax({
						url: '/withdrawals/month',
						method: 'POST',
						data: {month: $('#month').val(), year: $('#month-year').val()},

						beforeSend: function(){
							$('.table-container').html('<h3 class="label text-center">Please wait...</h3>').show();
						},

						success: function(response){
							$('.table-container').html(response).show();
							$('.report-table').DataTable({
								"scrollX": true,
								"dom": "Blftp",
								buttons: [{
									extend: 'pdfHtml5',
									text: 'PRINT',
									className: 'btn btn-primary bg-primary text-white',
									orientation: 'landscape',
									pageSize: 'LEGAL',
									download: 'open',
									fontSize: 14,
									title: 'Withdrawal Summary for ' + $('#month option:selected').text() + ', ' + $('#month-year').val(),

								}],
								columnDefs: [
									{ "targets": "office-col", "searchable": false}
								]
							});
						}

					});
				
				}
				// $.ajax()
			}else if(reportType==2){
					if(($('#year').val()>=1990 && $('#year').val()!=null)){
						$.ajax({
							url: '/withdrawals/year',
							method: 'POST',
							data: {year: $('#year').val()},

							beforeSend: function(){
								$('.table-container').html('<h3 class="label text-center">Please wait...</h3>').show();
							},

							success: function(response){
								$('.table-container').html(response).show();
								$('.report-table').DataTable({
									"scrollX": true,
									"dom": "Blftp",
									buttons: [{
										extend: 'pdfHtml5',
										text: 'PRINT',
										className: 'btn btn-primary bg-primary text-white',
										orientation: 'landscape',
										pageSize: 'LEGAL',
										download: 'open',
										fontSize: 14,
										title: 'Withdrawal Summary for '  + $('#month-year').val(),

									}],
									columnDefs: [
										{ "targets": "office-col", "searchable": false}
									]
								});
							}

						});
					}else{
					}
				}else{

				}
		});
	</script>
@endsection
