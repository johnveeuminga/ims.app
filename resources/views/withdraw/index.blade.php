@extends('layouts.master')
@section('content')
	@include('components.navbar')
	<div class="wrapper">
		@include('components.sidebar')
	</div>
	<div class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h4 class="page-title">
						Withdrawals 
					</h4>
					<div class="dashboard-section">
						@if(session('success'))
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									<span class="sr-only">Close</span>
								</button>
								<strong>Success!</strong> {{session('success')}}
							</div>
						@endif
							<div class="col-md-12">
								<hr>
									<strong>{{Auth::user()->office_id == 6 ? 'All Withdrawals': 'Your Withdrawals'}}</strong>
								<hr>
								<a class="btn btn-primary mb-3" href="/withdraw/create" role="button">Withdraw</a>
								<table class="table table-striped withdraw-table">
									<thead>
										<tr>
											@if(Auth::user()->office_id ==6)
												<th>#</th>
											@endif
											<th>Date</th>
											@if(Auth::user()->office_id ==6)
												<th>Office</th>
											@endif
											<th>Number of Items</th>
											<th class="text-center"> Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($withdrawals as $withdrawal)
										<tr>
											@if(Auth::user()->office_id==6)
												<th>{{$withdrawal->id}}</th>
											@endif
											<td>{{$withdrawal->formatDate($withdrawal->created_at)}}</td>
											@if(Auth::user()->office_id==6)
												<td>{{$withdrawal->office->office_name}}</td>
											@endif
											<td class="amount">{{count($withdrawal->items)}}</td>
											<td class="text-right">
												<a class="btn btn-primary" href="{{'/withdraw/'.$withdrawal->id}}" role="button">VIEW</a>
												<a class="btn btn-success" href="{{'/withdraw/'.$withdrawal->id.'/edit'}}" role="button">EDIT</a>
												@if(Auth::user()->office_id == 6)
													<a class="btn btn-info" href="{{'/withdraw/'.$withdrawal->id.'/print'}}" role="button">PRINT</a>
													<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#remove-modal" data-id={{$withdrawal->id}}>DELETE</button>

												@endif
												
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@if(Auth::user()->office_id == 6)
		@component('components.modal')
			@slot('id')
				"remove-modal"
			@endslot
			@slot('title')
				Delete Withdrawal
			@endslot
			@slot('body')
				Are you sure you want to delete this withdrawal? This process is irreversible.
				<form id="delete-form" method="POST">
					{{csrf_field()}}
					{{method_field('DELETE')}}
				</form>
			@endslot
			@slot('footer')
				<input class="btn btn-danger" type="submit" value="DELETE" form="delete-form">
			@endslot
		@endcomponent
		@component('components.modal')
			@slot('id')
				"complete-modal"
			@endslot
			@slot('title')
				Mark as Complete
			@endslot
			@slot('body')
				Are you sure this withdrawal has been processed?
				<form id="complete-form" method="POST">
					{{csrf_field()}}
					{{method_field('PUT')}}
				</form>
			@endslot
			@slot('footer')
				<input class="btn btn-primary" type="submit" value="SUBMIT" form="complete-form">
			@endslot
		@endcomponent
	@endif
@endsection

@section('scripts')
	<script>
		$('#withdraw-items').addClass('active');
		$('.withdraw-table').DataTable({
			"sDom": "lftp",
			columnDefs: [
				{ "targets": "amount", "searchable": false}
			]
		});

		@if(Auth::user()->office_id == 6)
			$('#remove-modal').on('show.bs.modal', function (event){
				var button = $(event.relatedTarget)
				var id = button.data('id')

				var modal = $(this)
				modal.find('.modal-header').addClass('bg-danger');
				modal.find('#delete-form').attr('action','/withdraw/'+id);
			});
		@endif
	</script>
@endsection