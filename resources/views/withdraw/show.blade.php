@extends('layouts.master')

@section('content')
	@include('components.navbar')
	<div class="wrapper">
		@include('components.sidebar')
	</div>
	<div class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="page-title text-center">
						REQUISITION AND ISSUE SLIP<br>
						<span class="title-small">Philippine Military Academy<br>(Agency)</span>
					</h2>
					<div class="form-container">
						<div class="row form-header mb-3">
							<div class="col-md-12">
								<div class="form-inline">
									<div class="form-group">
										<label for="exampleInputName2">Date: </label>
										<input type="text" class="form-control ml-2" id="display-date">
										<input type="hidden" name="withdrawDate" id="withdraw-date">
									</div>
									<div class="form-group">
										<label for="select-office">Office: </label>
										<input type="text" class="form-control ml-2" id="display-date" value="{{$withdrawal->office->office_name}}" disabled>
									</div>
									<div class="col-md-12 mt-4">
										<table class="table table-bordered">
										<thead>
											<tr>
												<th>Item</th>
												<th>Unit</th>
												<th>Quantity</th>
											</tr>
										</thead>
										<tbody>
											@foreach($withdrawal->items as $item)
												<tr>
													<th>{{$item->item->item_name}}</th>
													<td>{{$item->item->unit}}</td>
													<td>{{$item->amount}}</td>
												</tr>
											@endforeach
										</tbody>
									</table>
									<div class="float-sm-left button-container">
										<a class="btn btn-primary" href="/withdraw/{{$withdrawal->id}}/print" role="button">PRINT</a>
										<a class="btn btn-success" href="/withdraw/{{$withdrawal->id}}/edit" role="button">EDIT</a>
										<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#remove-modal" data-id={{$withdrawal->id}}>DELETE</button>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@if(Auth::user()->office_id == 6)
		@component('components.modal')
			@slot('id')
				"remove-modal"
			@endslot
			@slot('title')
				Delete Withdrawal
			@endslot
			@slot('body')
				Are you sure you want to delete this withdrawal? This process is irreversible.
				<form id="delete-form" method="POST" action="/withdraw/{{$withdrawal->id}}">
					{{csrf_field()}}
					{{method_field('DELETE')}}
				</form>
			@endslot
			@slot('footer')
				<input class="btn btn-danger" type="submit" value="DELETE" form="delete-form">
			@endslot
		@endcomponent
	@endif
@endsection

@section('scripts')
	<script>
		$('#withdraw-items').addClass('active');
		date = $('#display-date').datepicker({
			altField: '#withdraw-date',
			altFormat: 'yy-mm-dd',
			dateFormat: 'MM d, yy',
		});
		date.datepicker('setDate', new Date("{{$withdrawal->created_at}}"));
		date.datepicker('disable');
		$('#remove-modal .modal-header').addClass('bg-danger');
	</script>
@endsection