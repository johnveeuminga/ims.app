@extends('layouts.master')

@section('content')
	@include('components.navbar')
	<div class="wrapper">
		@include('components.sidebar')
	</div>
	<div class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="page-title text-center">
						REQUISITION AND ISSUE SLIP<br>
						<span class="title-small">Philippine Military Academy<br>(Agency)<br>Edit</span>
					</h2>
					<hr>

					<div class="form-container">
						<form method="POST" action="{{url('/withdraw/').'/'.$withdrawal->id}}">
							{{csrf_field()}}
							{{method_field('PUT')}}
							<div class="row form-header mb-3">
								<div class="col-md-12">
									<div class="form-inline">
										<div class="form-group">
											<label for="exampleInputName2">Date: </label>
											<input type="tesxt" class="form-control ml-2" id="display-date">
											<input type="hidden" name="withdrawDate" id="withdraw-date">
										</div>
										<div class="form-group">
											<label for="select-office">Office: </label>
											<select id="select-office" name="office">
												<option value="">Search for an office..</option>
											    @foreach($offices as $office)
											    	<option value="{{$office->id}}">{{$office->office_name}}</option>
											    @endforeach
											</select>	
										</div>
										@if(Auth::user()->office_id == 6)
											<div class="form-group ml-3">
												<label for="select-office">Created By: </label>
												<select id="select-user" name="user">
													<option value="">Search for an user..</option>
												    @foreach($users as $user)
												    	<option value="{{$user->id}}">{{$user->name}}</option>
												    @endforeach
												</select>	
											</div>
										@endif
									</div>
								</div>
							</div>
							
								<div class="row">
									<div class="col-md-6">
										<div class="font-weight-bold">Search:</div>
										<div class="">
											<select class="select-item">
												<option value="">Search for an item to add..</option>
												@foreach($items as $item)
													<option value="{{$item->id}}">{{$item->item_name}}</option>
												@endforeach
											</select>
											<button class="btn btn-primary" type="button" id="add-item">Add</button>
										</div>
									</div>
								</div>
								
								<div class="col-md-12 mt-4">
									<table class="table table-striped" id="withdraw-table">
										<thead>
											<tr>
												<th>Item</th>
												<th>Unit</th>
												<th>Quantity</th>
												<th>Remove</th>
											</tr>
										</thead>
										<tbody>
											@foreach($withdrawal->items as $item)
												<tr>
													<th>{{$item->item->item_name}}</th>
													<td>{{$item->item->unit}}</td>
													<td><input type='number' class='form-control qty' min='1' max="{{$item->item->amount}}" name='oldqty[]' step='1' id="qty{{$item->item->id}}" value="{{$item->amount}}"/></td>
													<td><button type="button" data-toggle="modal" data-target="#remove-modal" class="btn btn-danger" data-item-id="{{$item->id}}">Remove</button></td>
													<input type="hidden" name="olditems[]" value="{{$item->item->id}}">
													<input type="hidden" name="oldWithdrawItemId[]" value="{{$item->id}}">
												</tr>
											@endforeach
										</tbody>
									</table>
									<div class="float-sm-right button-container">
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	@component('components.modal')
		@slot('id')
			"remove-modal"
		@endslot
		@slot('title')
			Delete User
		@endslot
		@slot('body')
			Are you sure you want to delete this item line? This process is irreversible.
			<form id="delete-form" method="POST">
				{{csrf_field()}}
				{{method_field('DELETE')}}
			</form>
		@endslot
		@slot('footer')
			<input class="btn btn-danger" type="submit" value="DELETE" form="delete-form">
		@endslot
	@endcomponent
@endsection

@section('scripts')
	<script>
		$('#withdraw-items').addClass('active');

		date = $('#display-date').datepicker({
			altField: '#withdraw-date',
			altFormat: 'yy-mm-dd',
			dateFormat: 'MM d, yy',
		});
		date.datepicker('setDate', new Date("{{$withdrawal->created_at}}"));
		officeselect = $('#select-office').selectize();
		officeSelectize = officeselect[0].selectize;
		officeSelectize.setValue("{{$withdrawal->office_id}}");
		@if(Auth::user()->office_id == 6)
			userselect = $('#select-user').selectize();
			userSelectize = userselect[0].selectize;
			userSelectize.setValue("{{$withdrawal->user_id}}");
		@endif
		itemselect = $('.select-item').selectize({
		});
		$('#add-item').click(function(){
			itemSelectize  = itemselect[0].selectize;
			id = parseInt(itemSelectize.getValue());
			switch(id){
				@foreach($items as $item)
					case {{$item->id}}:
						if($("#qty{{$item->id}}").length == 0){
							$('#withdraw-table tbody').append(
								"<tr id='{{$item->id}}'>"+
									'<th>'+"{{$item->item_name}}"+'</th>'+
									"<td>{{$item->unit}}</th>" + 
									"<td><input type='number' class='form-control qty' min='1' name='newqty[]' step='1' id=qty"+{{$item->id}}+" value='1' max='{{$item->amount}}'/>"+
									"<td><button type='button' class='btn btn-danger btn-remove' data-tr-target='{{$item->id}}'>Remove</button></td>"+
								'</tr>'+
								'<input type="hidden" value="{{$item->id}}" name="newitems[]" />'
							);
						}else{
							console.log('add');
							if($('#qty{{$item->id}}').val().length ==0){
								$("#qty{{$item->id}}").val(1);
							}else{
								$("#qty{{$item->id}}").val( function(i, oldval) {
								    return ++oldval;
								});
							}
						}
						break;
						
				@endforeach
			}
		});
		$('#remove-modal').on('show.bs.modal', function (event){
			var modal = $(this)
			var button = $(event.relatedTarget)
			if(button.data('tr-target')){
				var id = button.data('tr-target');
				$('#withdraw-table #'+id).remove();
			}else{
				id = button.data('item-id')
				modal.find('#delete-form').attr('action','/users/'+id);

			}

			modal.find('.modal-header').addClass('bg-danger');
		});

		$(document).on("click", 'button.btn-remove', function(e){
			var id = ($(this).data('tr-target'));
			$('#withdraw-table #'+id).remove();
		});
	</script>
@endsection