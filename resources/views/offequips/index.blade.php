@extends('layouts.master')

@section('content')
	@include('components.navbar')
	<div class="wrapper">
		@include('components.sidebar')
	</div>
	<div class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12 h-100">
					@php
						$i=0;
					@endphp
					<h4 class="page-title">OFFICE EQUIPMENTS</h4>
					<hr>
					<div class="dashboard-section">
						<a class="btn btn-primary mb-3" href={{url('withdraw')}} role="button">Withdraw</a>
						<table class="table table-striped data-tables">
							<thead>
								<tr>
									<th>Item</th>
									<th class="text-center">Total</th>
									@if(Auth::user()->office_id ==6)
										<th>Danger Level</th>
										@foreach($offices as $office)
											<th>{{$office->office_name}}</th>
										@endforeach
									@endif
								</tr>
							</thead>
							<tbody>
								@foreach($items as $item)
								<tr>
									<th>{{$item->item_name}}</th>
									<th class="text-center">{{$item->amount}}</th>
									@if(Auth::user()->office_id == 6)
										<th class="text-center">{{$item->stock_danger}}</th>
										@foreach($offices as $office)
											@php
												$i=0;
											@endphp
											<td class="office-col text-center">
												@foreach($office->withdrawals as $withdrawal)
													@foreach($withdrawal->items as $withdrawalItem)
														@if($withdrawalItem->item_id == $item->id)
															@if($withdrawalItem->item->reorder_date)
																@if(\Carbon\Carbon::parse($withdrawalItem->created_at)->gte(\Carbon\Carbon::parse($withdrawalItem->item->reorder_date)))
																	@php
																		$i += $withdrawalItem->amount;
																	@endphp
																@endif
															@else
																@php
																	$i += $withdrawalItem->amount;
																@endphp
															@endif
														@endif
													@endforeach
												@endforeach
												@if(!$i==0)
													{{$i}}
												@endif
											</td>
										@endforeach
									@endif
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		$('#office-supplies').addClass('active');
	</script>
@endsection