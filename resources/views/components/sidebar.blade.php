<div class="sidebar navbar-inverse bg-main-light h-100">
    <ul class="nav menu flex-column mx-auto">
        @if(Auth::user()->office_id==6)
            <li class="nav-item" id="dashboard">
                <a href='{{url("/home")}}' class="nav-link">
                    <i class="fa fa-home"></i>
                    Home
                </a>
            </li>
        @endif
        <li class="nav-item" id="withdraw-items">
            <a href='{{url("/withdraw")}}' class="nav-link">
                <i class="fa fa-hand-paper-o"></i>
                Withdraw
                @if(Auth::user()->office_id ==7 )
                <div class="notification float-sm-right"></div>
                @endif
            </a>
        </li>
        <li class="nav-item" id="office-supplies">
            <a href='{{url("/officesupplies")}}' class="nav-link">
                <i class="fa fa-folder-open"></i>
                Office Supplies
            </a>
        </li>
        <li class="nav-item" id="equipments">
            <a href='{{url("/equipments")}}' class="nav-link">
                <i class="fa fa-wrench"></i>
                Equipments
            </a>
        </li>
        <li class="nav-item" id="janitorial-supplies">
            <a href='{{url("/janitorialsupplies")}}' class="nav-link">
                <i class="fa fa-bath"></i>
                Janitorial Supplies
            </a>
        </li>
        @if(Auth::user()->office_id == 6)
             <li class="nav-item" id="reorder">
                <a class="nav-link" href="{{url('/reorder')}}">
                    <i class="fa fa-refresh"></i>
                    Resupply
                </a>
            </li>
        @endif
        @if(Auth::user()->office_id == 6)
            <li class="nav-item" id="user-management">
                <a href='/users' class="nav-link">
                    <i class="fa fa-group"></i>
                    User Management
                </a>
            </li>
             <li class="nav-item" id="items">
            <a class="nav-link" href="{{url('/itemmanagement')}}">
                    <i class="fa fa-gear"></i>
                    Item Management
                </a>
            </li>
        @endif
        @if(Auth::user()->office_id == 6)
             <li class="nav-item" id="reports">
                <a class="nav-link" href="{{url('/reports')}}">
                    <i class="fa fa-line-chart"></i>
                    Reports
                </a>
            </li>
        @endif
    </ul>
</div>