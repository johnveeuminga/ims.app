<nav class="navbar navbar-toggleable-md navbar-inverse bg-main fixed-top">
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	</button>

	<a class="navbar-brand" href='{{url("/home")}}'>O/ACAD4 E-INVENTORY MANAGEMENT SYSTEM</a>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<div class="nav navbar-nav float-md-right ml-auto">
			@if(Auth::user()->office_id==6)
				<li class="dropdown" id="notifications-drop">
					<a class="dropdown-toggle" href="#" id="notifications" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fa fa-bell-o"></i>
						<span class="badge">{{count(Auth::user()->unreadNotifications) == 0 ? '': count(Auth::user()->unreadNotifications)}}</span>
					</a>
					<div class="dropdown-menu" id="notifications-dropdown" aria-labelledby="notifications">
						<h4 class="dropdown-header">NOTIFICATIONS<span class="notification-tag float-md-right">{{count(Auth::user()->unreadNotifications) == 0 ? '0 UNREAD': count(Auth::user()->unreadNotifications).' UNREAD'}}</span></h4>
						@foreach(Auth::user()->notifications as $notification)
							<a class="dropdown-item {{$notification->read_at == null ? 'notif-unread':''}}" href="{{url('withdraw/notification')}}/{{$notification->id}}">
								<h6 class="font-weight-bold mb-2">New Withdrawal!</h6>
								<p><span class="font-weight-bold">{{$notification->data['office']['office_name']}}</span> has withdrawn</p>
							</a>
						@endforeach

					</div>
				</li>
			@endif
			
			<li class="dropdown">
			 	<a class="dropdown-toggle" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    {{Auth::user()->office->office_name}}
			 	</a>
			 	
				<div class="dropdown-menu" id="user-form" aria-labelledby="dropdownMenuLink">
					<a class="dropdown-item" href="/users/{{Auth::user()->id}}/changepass">
						<i class="fa fa-lock"></i>
						Change Password
					</a>
					<div class="dropdown-divider"></div>
	                <a class="dropdown-item" href="{{ url('/logout') }}"
	                    onclick="event.preventDefault();
	                             document.getElementById('logout-form').submit();">
						<i class="fa fa-power-off"></i>
	                    
	                    Logout
	                </a>

	                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
	                    {{ csrf_field() }}
	                </form>

				</div>
			</li>
		 	
		</div>
	</div>
</nav>

