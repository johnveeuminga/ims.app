@extends('layouts.master')

@section('content')
	@include('components.navbar')
	<div class="wrapper">
		@include('components.sidebar')
	</div>
	<div class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title text-center">Withdraw Items</h3>
					<hr>

					<div class="form-container">
						@if($errors->any())
							<div class="alert alert-danger" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								    <span aria-hidden="true">&times;</span>
								 </button>
							    <strong>Warning!</strong> Withdrawal form cannot be saved.
							    <ul>
							    	@foreach($errors->all() as $message)
							    		<li>{{$message}}</li>
							    	@endforeach
							    </ul>
							</div>
						@endif
						<form method="POST" action="{{url('/withdraw')}}">
							{{csrf_field()}}
							<div class="row form-header mb-3">
								<div class="col-md-12">
									<div class="form-inline">
										<div class="form-group">
											<label for="exampleInputName2">Date: </label>
											<input type="tesxt" class="form-control ml-2" id="display-date">
											<input type="hidden" name="withdrawDate" id="withdraw-date">
										</div>
										@if(Auth::user()->office_id == 6)
											<div class="form-group">
												<label for="select-office">Office: </label>
												<select id="select-office" name="office">
													<option value="">Search for an office..</option>
												    @foreach($offices as $office)
												    	<option value="{{$office->id}}" {{$office->id == Auth::user()->office_id ? 'selected': ''}}>{{$office->office_name}}</option>
												    @endforeach
												</select>	
											</div>
										@endif
									</div>
								</div>
							</div>
							
								<div class="row">
									<div class="col-md-6">
										<div class="font-weight-bold">Search:</div>
										<div class="">
											<select class="select-item">
												<option value="">Search for an item to add..</option>
												@foreach($items as $item)
													<option value="{{$item->id}}">{{$item->item_name}}</option>
												@endforeach
											</select>
											<button class="btn btn-primary" type="button" id="add-item">Add</button>
										</div>
									</div>
								</div>
								
								<div class="col-md-12 mt-4">
									<table class="table table-striped" id="withdraw-table">
										<thead>
											<tr>
												<th>Item</th>
												<th>Unit</th>
												<th>Quantity</th>
												<th>Remove</th>
											</tr>
										</thead>
										<tbody>
											
										</tbody>
									</table>
									<div class="float-sm-right button-container">
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</div>
							</div>
						</form>
						
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function(){
			$('#withdraw-items').addClass('active');
			$('#display-date').datepicker({
				altField: '#withdraw-date',
				altFormat: 'yy-mm-dd',
				dateFormat: 'MM d, yy',
			});
			@if(Auth::user()->office_id == 6)
				var selectOffice = $('#select-office').selectize();
				officeSeletize = selectOffice[0].selectize;

			@endif
			itemselect = $('.select-item').selectize({
			});

			$('#add-item').click(function(){
				itemSelectize  = itemselect[0].selectize;
				id = parseInt(itemSelectize.getValue());
				switch(id){
					@foreach($items as $item)
						case {{$item->id}}:
							if($("#qty{{$item->id}}").length == 0){
								$('#withdraw-table tbody').append(
									"<tr id='{{$item->id}}'>"+
										'<th>'+"{{$item->item_name}}"+'</th>'+
										"<td>{{$item->unit}}</th>" + 
										"<td><input type='number' class='form-control qty' min='1' name='qty[]' step='1' id=qty"+{{$item->id}}+" value='1' max="+{{$item->amount}}+" />"+
										"<td><button type='button' class='btn btn-danger btn-remove' data-tr-target='{{$item->id}}'>REMOVE</button></td>"+
										'<input type="hidden" value="{{$item->id}}" name="items[]" />'+
									'</tr>'
								);
							}else{
								console.log('add');
								if($('#qty{{$item->id}}').val().length ==0){
									$("#qty{{$item->id}}").val(1);
								}else{
									$("#qty{{$item->id}}").val( function(i, oldval) {
									    return ++oldval;
									});
								}
							}

							break;
					@endforeach

					
				}
		});

			$(document).on("click", 'button.btn-remove', function(e){
				var id = ($(this).data('tr-target'));
				$('#withdraw-table #'+id).remove();
			});
		});
		
		

	</script>
@endsection