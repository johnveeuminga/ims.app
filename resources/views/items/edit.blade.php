@extends('layouts.master')
@section('content')
	@include('components.navbar')
	<div class="wrapper">
		@include('components.sidebar')
	</div>
	<div class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
						ADD ITEM
					</h3>
					<hr>
					<div class="section">
						<h3 class="page-title text-center">Enter Item Information</h3>
						<div class="row">
							<div class="col-md-10 offset-md-1">
								@if($errors->any())
									<div class="alert alert-danger" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										    <span aria-hidden="true">&times;</span>
										 </button>
									    <strong>Warning!</strong> Item cannot be saved.
									    <ul>
									    	@foreach($errors->all() as $message)
									    		<li>{{$message}}</li>
									    	@endforeach
									    </ul>
									</div>
								@endif
								<form action="/itemmanagement/{{$item->id}}" method="POST">
									{{csrf_field()}}
									{{method_field('put')}}
									<input type="hidden" name="action" value="1">
									<div class="form-group row">
										<label for="Name" class="col-sm-2 form-control-label">Item Name:</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="name" name="item_name" placeholder="Item Name" value="{{$errors->any() ? old('item_name'): $item->item_name}}">
										</div>
									</div>
									<div class="form-group row">
										<label for="Name" class="col-sm-2 form-control-label">Unit:</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="name" name="unit" placeholder="Unit" value="{{$errors->any() ? old('unit'): $item->unit}}">
										</div>
									</div>
									<div class="form-group row">
										<label for="Name" class="col-sm-2 form-control-label">Initial Amount:</label>
										<div class="col-sm-10">
											<input type="number" min=0 step=1 class="form-control" id="name" name="amount" placeholder="Initial Amount" value="{{$errors->any() ? old('amount'): $item->amount}}">
										</div>
									</div>
									<div class="form-group row">
										<label for="Name" class="col-sm-2 form-control-label">Reorder Amount:</label>
										<div class="col-sm-10">
											<input type="number" min=0 step=1 class="form-control" id="name" name="reorder_amount" placeholder="Reorder Amount" value="{{$errors->any() ? old('reorder_amount'): $item->stock_danger}}">
										</div>
									</div>
									<div class="form-group row itemtype">
										<label for="offices" class="col-sm-2 form-control-label">Item Type:</label>
										<div class="col-sm-10">
											<select id="itemtype" name="itemtype">
												@foreach($itemtypes as $itemtype)
													<option value={{$itemtype->id}} {{$errors->any() ? old('itemtype') == $itemtype->id ? "selected":"" : $item->item_types_id == $itemtype->id ? 'selected': ''}}>{{$itemtype->item_type}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group row equipmenttype {{$item->item_types_id == 2 ? '': 'd-none'}}">
										<label for="offices" class="col-sm-2 form-control-label">Equipment Type:</label>
										<div class="col-sm-10">
											<select id="equipmenttype" name="equipmenttype">
												@foreach($equipmenttypes as $equipmenttype)
													<option value={{$equipmenttype->id}} {{$errors->any() ? old('equipmentype') == $equipmenttype->id ? "selected":"" : $item->equipment_type_id == $equipmenttype->id ? 'selected': ''}}>{{$equipmenttype->equipment_type}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-offset-2 col-sm-10">
											<button type="submit" class="btn btn-primary">SAVE</button>
										</div>
									</div>
								</form>
							</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script>

		$('#itemmanagement').addClass('active');
		var itemtype = $('#itemtype').selectize({});
		var equipmenttypes = $('#equipmenttype').selectize({});
		var itemtypeSelectize = itemtype[0].selectize;
		var officesSelectize = equipmenttypes[0].selectize;

		itemtypeSelectize.on('change', function() {
		    if(this.getValue()==2){
		    	$('.equipmenttype').removeClass('d-none');
		    }else{
		    	$('.equipmenttype').addClass('d-none');

		    }
		});
	</script>
@endsection