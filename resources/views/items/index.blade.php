@extends('layouts.master')
@section('content')
	@include('components.navbar')
	<div class="wrapper">
		@include('components.sidebar')
	</div>
	<div class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">Item Management</h3>
					<hr>
					<div class="dashboard-section">
						@if(session('success'))
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									<span class="sr-only">Close</span>
								</button>
								<strong>Success!</strong> {{session('success')}}
							</div>
						@endif
						@if($errors->any())
							<div class="alert alert-danger" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								    <span aria-hidden="true">&times;</span>
								 </button>
							    <strong>Warning!</strong> Item cannot be saved.
							    <ul>
						    		<li>{{$errors->first()}}</li>
							    </ul>
							</div>
						@endif
						<a class="btn btn-primary mb-3" href="/itemmanagement/create" role="button">ADD ITEM</a>
						<table class="table table-striped" id="item-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Item Name</th>
									<th>Itemtype</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($items as $item)
									<tr>
										<th>{{$item->id}}</th>
										<td>{{$item->item_name}}</td>
										<td>{{$item->itemtype->item_type}}</td>
										<td class="text-center">
											<a class="btn btn-primary" href="/itemmanagement/{{$item->id}}/edit" role="button">EDIT</a>
											<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#remove-modal" data-id={{$item->id}}>REMOVE</button>
										</td>

									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@component('components.modal')
		@slot('id')
			"remove-modal"
		@endslot
		@slot('title')
			Delete Item
		@endslot
		@slot('body')
			Are you sure you want to delete this item? This process is irreversible.
			<form id="delete-form" method="POST">
				{{csrf_field()}}
				{{method_field('DELETE')}}
			</form>
		@endslot
		@slot('footer')
			<input class="btn btn-danger" type="submit" value="DELETE" form="delete-form">
		@endslot
	@endcomponent
@endsection

@section('scripts')
	<script>
		$('#items').addClass('active');

		$('#item-table').DataTable({
			"sDom": "lftrp"
		})
		$('#remove-modal').on('show.bs.modal', function (event){
			var button = $(event.relatedTarget)
			var id = button.data('id')

			var modal = $(this)
			modal.find('.modal-header').addClass('bg-danger');
			modal.find('#delete-form').attr('action','/itemmanagement/'+id);
		});
	</script>
@endsection