<!DOCTYPE html>
<html>
<head>
	<title>Inventory Management System</title>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/css/datatables.css">
	<link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="/css/selectize.css">
	<link rel="stylesheet" type="text/css" href="/css/all.css">
	@yield('stylesheets')
	<script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
</head>
<body class="h-100 w-100">
	@yield('content')
	<script src="/js/jquery-3.1.1.min.js"></script>
	<script src="/js/tether.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/datatables.min.js"></script>
    <script src="/js/dataTables.buttons.min.js"></script>
    <script src="/js/buttons.print.min.js"></script>
    <script type="text/javascript" language="javascript" src="/js/pdfmake.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="/js/vfs_fonts.js">
	</script>
    <script src="/js/buttons.html5.min.js"></script>
    <script src="/js/buttons.flash.min.js"></script>
	<script src="/js/jquery-ui.min.js"></script>
	<script src="/js/selectize.js"></script>
	<script src="/js/app.js"></script>
	<script src="/js/notify.js"></script>


	<script>
		$('.data-tables').DataTable({
			@if(Auth::check())
				@if(Auth::user()->office_id == 6)
				"scrollX": true,
				"dom": "Blftp",
				buttons: [{
					extend: 'pdfHtml5',
					text: 'PRINT',
					className: 'btn main-btn btn-primary bg-primary text-white',
					orientation: 'landscape',
					pageSize: 'LEGAL',
					download: 'open',
					fontSize: 14,
					title: 'Inventory Overview' 

				}],
				@else
					"dom": "lftp",
				@endif
			@endif
			
			columnDefs: [
				{ "targets": "office-col", "searchable": false}
			]
		});
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
	</script>
	@if(Auth::check())
		<script>
			$.notify.addStyle('notif-box', {
    			html: "<div class='bg-primary'data-notify-html='title'>"+
    					
    				  "</div>",

    		});
			var notifications;
            Echo.private('withdrawals')
            	.listen('.new-withdrawal', (e) =>{
            		$.ajax({
            			url: "/users/{{Auth::user()->id}}/unreadNotifications",
            			success: function(data){
            				console.log(data);
            				$('.navbar .badge').html(data.length);
            				$('.navbar .notification-tag').html(data.length);
            				var append = ('<a class="dropdown-item notif-unread" href="/withdraw/notification/'+data[0].id+'">'
            								+'<h6 class="font-weight-bold mb-2">New Withdrawal!</h6>'
            									+'<p><span class="font-weight-bold">'+e.office.office_name+'</span> has withdrawn</p>'
            										+'</a>')
            				$(append).insertAfter("#notifications-drop .dropdown-header");

		            		$.notify({
								'title': '<a href="/withdraw/notification/'+data[0].id+'">'+
											'<h6 class="font-weight-bold mb-2">New Withdrawal!</h6>'
		            							+'<p><span class="font-weight-bold">'+data[0].data.office.office_name+'</span> has withdrawn</p>'+
										   '</a>'
							}, {
								position: "right bottom",
								style: 'notif-box',
								showAnimation: 'slideDown',
								showDuration: 400,
								hideAnimation: 'slideUp',
								hideDuration: '200'


							});


            			}
            		})
            		
            		console.log(e);
            	});
           
        </script>
        <script>
        </script>
	@endif
	@yield('scripts')
</body>
</html>