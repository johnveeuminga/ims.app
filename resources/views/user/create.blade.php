@extends('layouts.master')
@section('content')
	@include('components.navbar')
	<div class="wrapper">
		@include('components.sidebar')
	</div>
	<div class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
						ADD USER
					</h3>
					<hr>
					<div class="section">
						<h3 class="page-title text-center">Enter User Information</h3>
						<div class="row">
							<div class="col-md-10 offset-md-1">
								@if($errors->any())
									<div class="alert alert-danger" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										    <span aria-hidden="true">&times;</span>
										 </button>
									    <strong>Warning!</strong> User cannot be saved.
									    <ul>
									    	@foreach($errors->all() as $message)
									    		<li>{{$message}}</li>
									    	@endforeach
									    </ul>
									</div>
								@endif
								<form action="/users" method="POST">
									{{csrf_field()}}
									<input type="hidden" name="action" value="1">
									<div class="form-group row">
										<label for="username" class="col-sm-2 form-control-label">Username:</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="username" name="username" placeholder="Username" value = "{{old('username')}}">
										</div>
									</div>
									<div class="form-group row">
										<label for="password" class="col-sm-2 form-control-label">Password:</label>
										<div class="col-sm-10">
											<input type="password" name="password" class="form-control" id="password" placeholder="Password">
										</div>
									</div>
									<div class="form-group row">
										<label for="password_confimration" class="col-sm-2 form-control-label">Confirm password:</label>
										<div class="col-sm-10">
											<input type="password" name="password_confirmation" class="form-control" id="password_confimration" placeholder="Confirm Password">
										</div>
									</div>
									<div class="form-group row offices">
										<label for="offices" class="col-sm-2 form-control-label">Offices:</label>
										<div class="col-sm-10">
											<select id="offices" name="office">
												@foreach($offices as $office)
													<option value={{$office->id}} {{old('office') == $office->id ? "selected":""}}>{{$office->office_name}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-offset-2 col-sm-10">
											<button type="submit" class="btn btn-primary">SAVE</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script>
		$('#user-management').addClass('active');
		var offices = $('#offices').selectize({});
		var officesSelectize = offices[0].selectize;
	</script>
@endsection