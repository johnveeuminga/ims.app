@extends('layouts.master')

@section('content')
	@include('components.navbar')
	<div class="wrapper">
		@include('components.sidebar')
	</div>
	<div class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
						Change Password
					</h3>
					<hr>
					<div class="section">
						<div class="col-md-10 offset-md-1">
							@if(session()->has('oldpass'))
								<div class="alert alert-danger alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
										<span class="sr-only">Close</span>
									</button>
									<strong>{{session()->get('oldpass')}}</strong> 
								</div>
							@endif
							@if($errors->any())
								<div class="alert alert-danger" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									    <span aria-hidden="true">&times;</span>
									 </button>
								    <strong>Warning!</strong> Password cannot be changed.
								    <ul>
								    	@foreach($errors->all() as $message)
								    		<li>{{$message}}</li>
								    	@endforeach
								    </ul>
								</div>
							@endif
							@if(session('success'))
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									<span class="sr-only">Close</span>
								</button>
								<strong>Success!</strong> {{session('success')}}
							</div>
						@endif
							<form action="/users/{{Auth::user()->id}}/changepass" method="POST">
								{{csrf_field()}}
								{{method_field('PUT')}}
								<div class="form-group row">
									<label for="old-pass" class="col-sm-2 form-control-label">Old Password:</label>
									<div class="col-sm-10">
										<input type="password" class="form-control" id="old_password" name="old_password" placeholder="Old Password">
									</div>
								</div>

								<div class="form-group row">
									<label for="new-pass" class="col-sm-2 form-control-label">New Password:</label>
									<div class="col-sm-10">
										<input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password">
									</div>
								</div>

								<div class="form-group row">
									<label for="new-pass" class="col-sm-2 form-control-label">Confirm Password:</label>
									<div class="col-sm-10">
										<input type="password" class="form-control" id="new_password_confirmation" name="new_password_confirmation" placeholder="Confirm Password">
									</div>
								</div>

								<div class="form-group float-sm-right">
                        			<button type="submit" class="btn btn-primary">Save</button>
	                        	</div>
								
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection