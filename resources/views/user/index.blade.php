@extends('layouts.master')
@section('content')
	@include('components.navbar')
	<div class="wrapper">
		@include('components.sidebar')
	</div>
	<div class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">User Management</h3>
					<hr>
					<div class="dashboard-section">
						@if(session('success'))
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									<span class="sr-only">Close</span>
								</button>
								<strong>Success!</strong> {{session('success')}}
							</div>
						@endif
						@if($errors->any())
							<div class="alert alert-danger" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								    <span aria-hidden="true">&times;</span>
								 </button>
							    <strong>Warning!</strong> Withdrawal form cannot be saved.
							    <ul>
						    		<li>{{$errors->first()}}</li>
							    </ul>
							</div>
						@endif
						<a class="btn btn-primary mb-3" href="/users/create" role="button">ADD USER</a>
						<table class="table table-striped" id="user-table">
							<thead>
								<tr>
									<th>ID</th>
									<th>Username</th>
									<th>Office</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($users as $user)
									<tr>
										<th>{{$user->id}}</th>
										<td>{{$user->username}}</td>
										<td>
											{{$user->office->office_name}}
										</td>
										<td class="text-center">
											<a class="btn btn-success" href="/users/{{$user->id}}/edit" role="button">EDIT</a>
											<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#remove-modal" data-id={{$user->id}}>REMOVE</button>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@component('components.modal')
		@slot('id')
			"remove-modal"
		@endslot
		@slot('title')
			Delete User
		@endslot
		@slot('body')
			Are you sure you want to delete this user? This process is irreversible.
			<form id="delete-form" method="POST">
				{{csrf_field()}}
				{{method_field('DELETE')}}
			</form>
		@endslot
		@slot('footer')
			<input class="btn btn-danger" type="submit" value="DELETE" form="delete-form">
		@endslot
	@endcomponent
@endsection

@section('scripts')
	<script>

		$('#user-management').addClass('active');
		$('#user-table').DataTable({
			"sDom": "lftrp"
		})
		$('#remove-modal').on('show.bs.modal', function (event){
			var button = $(event.relatedTarget)
			var id = button.data('id')

			var modal = $(this)
			modal.find('.modal-header').addClass('bg-danger');
			modal.find('#delete-form').attr('action','/users/'+id);
		});
	</script>
@endsection