@extends('layouts.master')

@section('content')
    <div class="login h-100">

      <div class="container">
          <h2 class="page-title text-center">O/ACAD4 E-INVENTORY MANAGEMENT SYSTEM</h2>
          <div class="row">
            <div class="col-md-6">
              <img class="img-fluid d-block mx-auto logo" src="/images/logo-1.png" width=200 height=250 />
            </div>
            <div class="col-md-6">
              
              <form method ="POST" action="{{ url('/login') }}" id="login-form">
                  {{csrf_field()}}
                  <h3 class="login-logo">Sign In</h3>

                  <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                      <input id="username" type="text" class="form-control mb-2" name="username" value="{{ old('username') }}" required autofocus placeholder="Username">
                       @if ($errors->has('username'))
                           <span class="help-block">
                               <strong>{{ $errors->first('username') }}</strong>
                           </span>
                       @endif
                  </div>

                   <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                      <input id="password" type="password" class="form-control" name="password" required placeholder="Password"> 
                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                   </div>

                  <button type="submit" class="btn btn-success btn-block login-submit">SUBMIT</button>
                </form>
              </div>
            </div>
        </div>
    </div>
@endsection