@extends('layouts.master')

@section('stylesheets')
    <link rel="stylesheet" type="text/css" href="/css/chartist.css">
@endsection

@section('content')
    @include('components.navbar')
    <div class="wrapper">
        @include('components.sidebar')
    </div>
    <div class="content-wrapper">
        <div class="container">
            <div class="row dashboard-wrapper">
                <div class="container">
                    <div class="col-md-12">
                        <h3 class="page-title">HOME</h3>
                        <hr>
                        <div class="row widget-holder">
                            <div class="col-md-4">
                                <div class="card bg-danger">
                                    <div class="card-block">
                                        <div class="row">
                                            <div class="col-md-12 title text-center">
                                                <div class="huge">
                                                    {{$danger_items->where('item_types_id', 1)->count()}}
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                    Office supplies running low on stock.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card bg-primary">
                                    <div class="card-block">
                                        <div class="row">
                                            <div class="col-md-12 title text-center">
                                                <div class="huge">
                                                    {{$danger_items->where('item_types_id', 3)->count()}}
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                Janitorial supplies running low on stock.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card bg-success">
                                    <div class="card-block">
                                        <div class="row">
                                            <div class="col-md-12 title text-center">
                                                <div class="huge">
                                                    @if(count($mostOffice))
                                                        {{$mostOffice->office_name}}
                                                    @else
                                                        -
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12 text-center">
                                                Office with frequent withdrawals
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="dashboard-section">
                            <h4 class="page-title">ITEMS RUNNING LOW ON STOCK</h4>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Item Type</th>
                                        <th>Stock Reorder</th>
                                        <th>Available Stock</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($danger_items as $danger_item)
                                    <tr>
                                        <th>{{$danger_item->item_name}}</th>
                                        <td>{{$danger_item->itemtype->item_type}}</td>
                                        <td>{{$danger_item->stock_danger}}</td>
                                        <td>{{$danger_item->amount}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <hr>
                        <div class="dashboard-section">
                            <h4>WITHDRAWAL OF HAG OFFICES/SUB-UNITS</h4>
                            <div class="col-md-12">
                                <div class="ct-chart ct-office-overview"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="/js/chartist.min.js"></script>
    <script>
        var officeData = {
            labels: {!!json_encode($office)!!},
            series: [{!!json_encode($officecount)!!}]
        };
        var options = {
           axisY: {
                onlyInteger: true,
            },
            height: 400
        };
        new Chartist.Bar('.ct-office-overview', officeData, options );

        $('#dashboard').addClass('active');

    </script>
@endsection

    