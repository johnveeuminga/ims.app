const { mix } = require('laravel-mix');

mix.browserSync('ims.app');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js');
mix.styles([
	'resources/assets/css/main.css',
	'resources/assets/css/sidebar.css',
	'resources/assets/css/login.css',
	'resources/assets/css/dashboard.css',
	'resources/assets/css/withdraw.css',
	], 'public/css/all.css');