<?php

use Illuminate\Support\Facades\Redis;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@showLoginForm');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('/officesupplies', 'OfficeEquipmentsController');
Route::resource('/withdraw', 'WithdrawController');
Route::get('/withdraw/notification/{id}/', 'WithdrawController@markAsRead');
Route::get('/withdraw/{id}/print', 'WithdrawController@pdf')->middleware('admin');
Route::put('/withdraw/{id}/complete', 'WithdrawController@complete')->middleware('admin');
Route::resource('/equipments', 'EquipmentsController');
Route::resource('/janitorialsupplies', 'JanitorialSuppsController');
Route::resource('/users', 'UserController');
Route::get('/users/{id}/changepass', 'UserController@changePasswordForm');
Route::put('/users/{id}/changepass', 'UserController@changePassword');
Route::get('/users/{id}/unreadNotifications', 'UserController@getNotifications');
Route::post('/users/{id}/markNotifsRead', 'UserController@markAllNotifcationsRead');
Route::get('/reports', 'ReportsController@index');
Route::post('/withdrawals/month', 'ReportsController@getWithdrawalsOnMonth');
Route::post('/withdrawals/year', 'ReportsController@getWithdrawalsOnYear');
Route::get('/items/get/{id}', 'ItemController@getitem');
Route::get('/reorder', 'ReorderController@index');
Route::post('/reorder', 'ReorderController@store');
Route::resource('/itemmanagement', 'ItemManagementController');

Route::get('/test', function(){
	$data = [
		'event' => 'Withdraw',
		'data' =>  [
			'username' => 'John Vee'
		],
	];

	Redis::publish('test-withdraw', json_encode($data));
	return 'Done';
});
