<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
    public function itemtype(){
    	return $this->belongsTo('App\ItemType', 'item_types_id');
    }

    public function equipmenttype(){
    	return $this->belongsTo('App\EquipmentType', 'equipment_type_id');
    }

    public function officeItems(){
    	return $this->hasMany('App\OfficeItem');
    }

}
