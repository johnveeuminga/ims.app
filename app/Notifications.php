<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    //
    public $id;

    protected $table = 'notifications';

    public function getId(){
    	return $this->id;
    }
}
