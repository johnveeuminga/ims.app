<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    //
    public function withdrawals(){
    	return $this->hasMany('App\Withdraw');
    }

    public function employees(){
    	return $this->hasMany('App\Employee');
    }

    public function users(){
    	return $this->hasMany('App\User');
    }
}
