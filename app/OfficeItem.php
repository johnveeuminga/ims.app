<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficeItem extends Model
{
    //
    protected $fillable = ['date', 'item_id', 'amount', 'withdraw_id'];

    public function item(){
    	return $this->belongsTo('App\Item', 'item_id');
    }

    public function form(){
    	return $this->belongsTo('App\Withdraw', 'withdraw_id');
    }
}
