<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    //
    public function admins()
    {
        return $this->morphMany('App\User', 'userable');
    }
}
