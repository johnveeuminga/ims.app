<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;

class WithdrawalCreated extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $withdrawal;
    public $user;
    public $office;
    public $notif_id;

    public function __construct($withdrawal, $user, $office)
    {
        //
        $this->user = $user;
        $this->withdrawal = $withdrawal;
        $this->office = $office;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
            'withdrawal' => $this->withdrawal,
            'user' => $this->user,
            'office' => $this->office,
        ];
    }
}
