<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    public function employees()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function office(){
    	return $this->belongsTo('App\Office');
    }
}
