<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Events\WithdrawDeleting;

class Withdraw extends Model
{
    // use SoftDeletes;
    //
    protected $fillable = ['office_id', 'user_id', 'status'];
    // protected $events = [
    //     'deleting' => WithdrawDeleting::class
    // ];

     protected $dates = ['deleted_at'];

    public function items(){
    	return $this->hasMany('App\OfficeItem');
    }

    public function office(){
    	return $this->belongsTo('App\Office');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function formatDate($date){
    	$dt = Carbon::parse($date);

    	return $dt->format('d F, Y');
    }


}
