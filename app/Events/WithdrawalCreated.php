<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class WithdrawalCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $user;
    public $withdrawals, $unreadWithdrawals;
    public $notifs;
    public $office;

    public function __construct($user, $withdrawals, $notifs, $office)
    {
        //
        $this->user = $user;
        $this->office = $office;
        $this->notifs = $notifs;
        $this->withdrawals = $withdrawals;
        

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('withdrawals');
    }

    public function broadcastAs(){
        return 'new-withdrawal';
    }
}
