<?php

namespace App\Http\Controllers;

use App\Events\NewWithdrawal;

use Illuminate\Support\Facades\Redis;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Item;

use App\Office;

use App\OfficeItem;

use App\Withdraw;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->office_id==6){

            $officeIndex = array();
            $officeTotal = array();
            $danger_items = Item::whereRaw('amount <= stock_danger')->get();
            $officeitem = OfficeItem::all();
            $withdraws = Withdraw::all();
            // dd($withdraws);
            $mostOffice = $withdraws->groupBy('office_id')->sortByDesc(function($rec){
                $key;
                // dd($rec);
                foreach($rec as $officeIndex=>$officeWithdraw){
                    $key = 0;
                    foreach ($officeWithdraw->items as $key => $withdrawalItem) {
                        $key += $withdrawalItem->amount;
                    }
                }
                return $key;
            });
            if(!is_null($mostOffice)){
                if($mostOffice->first()){
                    $mostOffice = $mostOffice->first()->first()->office;
                }
            }

            $mostItem = ($officeitem->groupBy('item_id')->sortByDesc(function($officeitem){
                return $officeitem->count();
            }));
            if($mostItem->first()){
                $mostItem = $mostItem->first()->first()->item;
            }
            $office = Office::all();
            $officenames = array();
            $officecount = array();
            foreach ($office as $key => $off) {
                $officenames[] = $off->office_name;
                if(!count($off->withdrawals)){
                    $officecount[] = 0;
                }else{
                     foreach($off->withdrawals as $index=>$withdrawals){
                        $officecount[$key] = 0;
                        foreach ($withdrawals->items as $withdrawalItem) {
                            $officecount[$key] += $withdrawalItem->amount;
                        }
                    }
                }
               
            }

            return view('home')->with('danger_items', $danger_items)->with('mostOffice', $mostOffice)->with('mostItem', $mostItem)->with('office', $officenames)->with('officecount', $officecount);
        }

        return redirect('/withdraw');
    }

    public function showLoginForm(){
        if(Auth::check()){
            return redirect('/home');
        }

        return redirect('/login');
    }
}
