<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Employee;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UserRequest;
use App\Office;
use App\User;
use App\Usertype;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    public function __construct(){
        $this->middleware('admin')->except(['changePasswordForm', 'changePassword']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::all();
        return view('user.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $usertypes = new User;
        $offices = Office::all();
        return view('user.create', ['usertypes' => $usertypes->usertypes, 'offices' => $offices]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        
        $user = new User;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->office_id = $request->office;

        $user->save();

        return redirect('/users')->with('success', 'User has been added succesfuly.');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $usertypes = new User;
        $offices = Office::all();

        return view('user.edit', ['user' => $user, 'usertypes' => $usertypes->usertypes, 'offices' => $offices]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        //
        $user = User::find($id);
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->office_id = $request->office;

        $user->save();
        return redirect('/users')->with('success', 'User has been updated succesfuly.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);

        $user->delete();

        return redirect('/users')->with('success', 'User has been deleted succesfuly.');

    }

    public function changePasswordForm($id){
        return view('user.changepassword', ['user' => User::find(Auth::user()->id)]);
    }

    public function changePassword(ChangePasswordRequest $request, $id){
        if(!Hash::check($request->old_password, Auth::user()->password)){
            return redirect('/users/'.$id.'/changepass')
                ->withErrors(['oldpass'=>'Old Password is incorrect']);
        }

        $user = User::find(Auth::user()->id);
        $user->password = bcrypt($request->new_password);
        $user->save();

        return redirect('/users/'.$id.'/changepass')->with('success', 'Password changed succesfuly.');
    }

    public function getNotifications($id){
        return Auth::user()->unreadNotifications;
    }

}
