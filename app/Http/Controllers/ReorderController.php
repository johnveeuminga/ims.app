<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ReorderRequest;

use App\Item;

use Carbon\Carbon;

class ReorderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        return $this->middleware('admin');
    }
    public function index()
    {
        //
        $items= Item::all();
        return view('reorder.index', ['items'=> $items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ReorderRequest $request)
    {
        //
        if($request->items){
            foreach ($request->items as $key => $item) {
                $item = Item::find($item);
                $item->amount += $request->qty[$key];
                $item->reorder_date = Carbon::now();
                $item->save();
            }
        }

        return redirect('/reorder')->with('success', 'Items successfuly updated. Reorder more items below.');
    }

}
