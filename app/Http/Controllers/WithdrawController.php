<?php

namespace App\Http\Controllers;

use App\Events\WithdrawalCreated as WithdrawalCreatedEvent;
use App\Http\Requests\WithdrawRequest;
use App\Item;
use App\Notifications;
use App\Notifications\WithdrawalCreated;
use App\Office;
use App\OfficeItem;
use App\User;
use App\Withdraw;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use PDF;

class WithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth', ['only' => ['pdf', 'destroy']]);
    }

    public function index()
    {
        //
        $withdrawals;
        $users = User::where('office_id', 6)->get();

        if(!(Auth::user()->office_id == 6)){
            $withdrawals = Withdraw::where('user_id', Auth::user()->id)->get();
        }else{
            $withdrawals = Withdraw::all();
        }

        return view('withdraw.index')->with('withdrawals', $withdrawals);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $offices = Office::all();
        $items = Item::all();

        return view('components.withdraw')->with('items', $items)->with('offices', $offices);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WithdrawRequest $request)
    {
        //
        $office;
        $withdraw = new Withdraw;
        $withdraw->created_at = Carbon::parse($request->withdrawDate);
        if(Auth::user()->office_id == 6)
            $office = $withdraw->office_id = $request->office;
        else{
            $office = $withdraw->office_id = Auth::user()->office_id;
        }
        $withdraw->user_id = Auth::user()->id;
        $withdraw->save();

        foreach ($request->items as $key => $item) {;
            $withdrawItem = new OfficeItem;
            $withdrawItem->item_id = $item;
            $withdrawItem->amount = $request->qty[$key];
            $withdrawItem->withdraw_id = $withdraw->id;
            $withdrawItem->save();

            $item = Item::find($withdrawItem->item_id);
            $item->amount -= $withdrawItem->amount;
            $item->save();
        }

        $users = User::where('office_id', 6)->get();
        $office = $withdraw->user->office;
        foreach($users as $user){
            $user->notify(new WithdrawalCreated($withdraw, $user, $office));
        }

        $user = User::where('office_id', 6)->get()->first();

        $notifications = $user->unreadNotifications;

        event(new WithdrawalCreatedEvent(Auth::user(), $withdraw, $notifications, $office));

        return redirect('/withdraw')->with('success', 'Withraw successful.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        foreach(Auth::user()->unreadNotifications as $notification){

        }
        $withdrawal = Withdraw::find($id);
        return view('withdraw.show')->with('withdrawal', $withdrawal);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    //
        $withdrawal = Withdraw::find($id);
        $offices = Office::all();
        $items = Item::all();
        $users = User::all();

        return view('withdraw.edit', ['offices' => $offices, 'items' => $items, 'withdrawal'=>$withdrawal, 'users' => $users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $withdraw = Withdraw::find($id);
        $oldstatus = $withdraw->status;
        $withdraw->created_at = Carbon::parse($request->withdrawDate);
        if(Auth::user()->office_id == 6)
            $withdraw->office_id = $request->office;
        else{
            $withdraw->office_id = Auth::user()->office_id;
        }
        $withdraw->user_id = Auth::user()->id;
        $withdraw->save();

        if($request->oldWithdrawItemId){
            foreach ($request->oldWithdrawItemId as $key => $items) {
                $withdrawItem = OfficeItem::find($request->oldWithdrawItemId[$key]);
                $withdrawItem->item_id = $request->olditems[$key];
                $withdrawItem->amount = $request->oldqty[$key];
                $withdrawItem->save();

                $item = Item::find($withdrawItem->item_id);
                $item->amount -= $withdrawItem->amount;
                $item->save();
            }
        }
        
        if($request->newitems){
             foreach ($request->newitems as $key=> $item){
                $withdrawItem = new OfficeItem;
                $withdrawItem->item_id = $item;
                $withdrawItem->amount = $request->newqty[$key];
                $withdrawItem->withdraw_id = $withdraw->id;
                $withdrawItem->save();

                $item = Item::find($withdrawItem->item_id);
                $item->amount -= $withdrawItem->amount;
                $item->save();
            }
        }


        return redirect('/withdraw')->with('success', 'Withrawal edited successful.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $withdraw = Withdraw::find($id);

        $withdraw->delete();

        return redirect('/withdraw')->with('success', 'Withraw deletion successful.');
    }

    public function pdf($id){
        $withdrawal = Withdraw::find($id);
        $pdf = PDF::loadView('withdraw.pdf', ['withdrawal' => $withdrawal]);
        return $pdf->stream();
    }

    public function markAsRead($id){
        $notif = Notifications::find($id);
        $notif->read_at = Carbon::now();
        $notif->save();

        $data = json_decode($notif->data);
        return redirect('/withdraw/'.$data->withdrawal->id);

    }

}
