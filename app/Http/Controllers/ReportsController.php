<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\OfficeItem;

use App\Office;

use App\Item;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('reports.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getWithdrawalsOnMonth(Request $request){
        $items = Item::all();
        $offices = Office::all();
        $officeitems = OfficeItem::whereMonth('created_at', $request->month)->whereYear('created_at', $request->year)->orderBy('created_at','DESC')->get();
        return view('reports.table-helper', ['items' => $items, 'offices' => $offices, 'officeitems' => $officeitems]);
    }

    public function getWithdrawalsOnYear(Request $request){
        $items = Item::all();
        $offices = Office::all();
        $officeitems = OfficeItem::whereYear('created_at', $request->year)->orderBy('created_at','DESC')->get();
        return view('reports.table-helper', ['items' => $items, 'offices' => $offices, 'officeitems' => $officeitems]);
    }
}
