<?php

namespace App\Http\Controllers;

use App\EquipmentType;
use App\Http\Requests\ItemRequest;
use App\Item;
use App\ItemType;
use Illuminate\Http\Request;

class ItemManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $items = Item::all();

        return view('items/index', ['items'=>$items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $itemtypes = ItemType::all();
        $equipmenttypes = EquipmentType::all();

        return view('items/create', ['itemtypes'=>$itemtypes, 'equipmenttypes'=>$equipmenttypes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request)
    {
        //
       $item = new Item;
       $item->item_name = $request->item_name;
       $item->amount = $request->amount;
       $item->unit = $request->unit;
       $item->stock_danger = $request->reorder_amount;
       $item->item_types_id = $request->itemtype;
       if($request->itemtype==2){
            $item->equipment_type_id = $request->equipmenttype;
       }

       $item->save();

       return redirect('/itemmanagement')->with('success', 'Item has been added succesfuly.');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $itemtypes = ItemType::all();
        $equipmenttypes = EquipmentType::all();
       return view('items.edit', ['itemtypes'=>$itemtypes, 'equipmenttypes'=>$equipmenttypes])->with('item', Item::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ItemRequest $request, $id)
    {
        //
        $item = new Item;
       $item->item_name = $request->item_name;
       $item->amount = $request->amount;
       $item->unit = $request->unit;
       $item->stock_danger = $request->reorder_amount;
       $item->item_types_id = $request->itemtype;
       if($request->itemtype==2){
            $item->equipment_type_id = $request->equipmenttype;
       }else{
            $item->equipment_type_id = null;

       }

       return redirect('/itemmanagement')->with('success', 'Item has been added updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $item = Item::find($id);

        $item->delete();

        return redirect('/itemmanagement')->with('success', 'Item has been deleted succesfuly.');
    }
}
