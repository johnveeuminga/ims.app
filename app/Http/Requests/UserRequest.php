<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

use Illuminate\Support\Facades\Auth;

use App\User;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'office' => 'required',
            'username' => 'required',
        ];

        if($this->action == 1){
            $rules['username'] .= '|unique:users';
            $rules['password'] = 'required|confirmed';
        }else{
            $rules['username'] .= '|'.Rule::unique('users')->ignore($this->user_id);
            $rules['password'] = 'confirmed';
        }

        return $rules;
    }
}
