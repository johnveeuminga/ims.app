<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_name' => 'required',
            'itemtype' => 'required',
            'unit' => 'required',
            'reorder_amount' => 'required',
            'amount' => 'required|numeric',
            'equipment_type' => 'required_if:itemtype,3',
        ];
    }
}
