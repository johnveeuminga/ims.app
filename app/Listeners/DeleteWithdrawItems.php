<?php

namespace App\Listeners;

use App\Events\WithdrawDeleting;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteWithdrawItems
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WithdrawDeleting  $event
     * @return void
     */
    public function handle(WithdrawDeleting $event)
    {
        //
        foreach($event->withdrawal->items as $items){
            $items->delete();
        }
    }
}
