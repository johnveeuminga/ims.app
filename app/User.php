<?php

namespace App;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password', 'usertype',
    ];

    public $usertypes = [['id' => 1, 'usertype' => 'Administrator'], 
                         ['id' => 2, 'usertype' => 'Employee']];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function office(){
        return $this->belongsTo('App\Office');
    }

    public function usertype(){
        return $this->belongsTo('App\Usertype');
    }

    public function withdrawals(){
        return $this->hasMany('App\Withdrwals');
    }

    public function receivesBroadcastNotificationsOn()
    {
        return (new PrivateChannel('users.'.$this->id));
    }
}
